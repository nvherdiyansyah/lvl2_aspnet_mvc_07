﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace LVL2_ASPNet_MVC_07.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult Index()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReoirtUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            // Ini passing parameter dari Visual Studio atau codingan
            ReportParameter[] reportParameter = new ReportParameter[1];
            reportParameter[0] = new ReportParameter("FirstName", "Ken");
            report.ServerReport.ReportPath = "/RiportNawadata";
            report.ServerReport.SetParameters(reportParameter);
            report.ServerReport.Refresh();

            // Ini memakai panggilan langsung
            //report.ServerReport.ReportPath = "/RiportNawadata";
            ViewBag.ReportViewer = report;
            return View();
        }
    }
}